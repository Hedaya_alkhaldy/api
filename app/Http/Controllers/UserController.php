<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

class UserController extends Controller
{
    function getUser(Request $request){

        $users = User::all();
        if($request->has('search')){
            $users = User::where('id','=',"$request->search")->get();
        }
        return view('users.index',compact('users'));
    }
}
