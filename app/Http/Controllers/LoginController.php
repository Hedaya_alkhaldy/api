<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;


class LoginController extends Controller
{
    public function login(){
        return view('login');
    }

    public function loginApi(Request $request){

        $login = $request->validate([

            'email' => 'required|email',
            'password' => 'required|min:8',
    ]);



       if(!Auth::attempt($login )){

           return response(['message'=>'Invalid Login']);
       }
       $accessToken = Auth::user()->createToken('authToken')->accessToken;

        //return response(['user'=>Auth::user(),'access_token'=>$accessToken]);
        //return view('dashboard')->with('message','you are Authorized user');
        return view("home")->with('message','You are authrized user');

    }

    public function allUsers(Request $request){


        return view('layouts.main');
    }
}
