@extends('layouts.main')

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    </div>

<div class="row">
    <div class="card mx-auto">
        <div>
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif

        </div>

        <div class="card-header d-flex">
            <div class="row">
                <div class="col">
                    <form method="GET" action="{{route('users.all')}}">
                        <div class="form-row align-items-center">
                            <div class="col">
                                <input type="search" name="search" id="inlineFormInput" class="form-control mb-2 w-auto" placeholder="Enter User Id" />
                            </div>
                            <div class="col">
                            <button type="submit" class="btn btn-primary mb-2">
                                Search
                            </button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
            <a href="" class="btn btn-primary mb-2 ml-auto  ">Create</a>


        </div>

        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Manage</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <div class="d-flex">
                                <a href="" class="btn btn-success">Edit</a>
                                <form  method="POST" action="">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger ml-3">Delete</button>
                                </form>
                            </div>


                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
