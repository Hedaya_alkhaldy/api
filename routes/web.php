<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::post('/LoginApi','App\Http\Controllers\LoginController@login')->name('LoginApi');
Route::get('/LoginApi','App\Http\Controllers\LoginController@login')->name('LoginApi');


Route::post('/ApiLogin','App\Http\Controllers\LoginController@loginApi')->name('ApiLogin');
Route::get('/ApiLogin','App\Http\Controllers\LoginController@loginApi')->name('ApiLogin');

Route::get('/dashboardUser','App\Http\Controllers\LoginController@allUsers')->name('dashboardUser');

Route::get('getUser',[UserController::class,'getUser'])->name('users.all');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

require __DIR__.'/auth.php';
