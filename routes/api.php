<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::group(['prefix' => 'auth'], function() {
//     Route::post('login', [AuthController::class],'login');
//     Route::get('login', [AuthController::class],'login');
//     Route::post('register', [AuthController::class],'register');
//     Route::get('register', [AuthController::class],'register');


// });

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('/user')->group(function(){

    Route::post('/loginApi','App\Http\Controllers\LoginController@loginApi');
});

Route::get('getUser',[UserController::class,'getUser']);
